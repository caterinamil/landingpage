## Landing Page Luxottica

---

This is a mini training project for the creation of a responsive landing page.

No frameworks were used (ex: bootstrap).

Desktop and mobile version, landing also works for other devices.

## Project structure
- **css** --> this folder will be populated after the scss files are compiled 
- **img** --> Landing images
- **scss/index.scss** --> Landing style
- **index.html** --> HTML structure

Prerequisites: install npm on your local machine.

To view the landing just open the project, and from the root run the following command

```
npm install
```

Once the packages are installed run the command

```
npm run compile:scss
```


Then open **index.html** file into your browser.

---

## Repository

https://bitbucket.org/caterinamil/landingpage/src/master/ 

---

